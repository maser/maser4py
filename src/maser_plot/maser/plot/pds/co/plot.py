# -*- coding: utf-8 -*-
from ..plot import Pds3Plot


class CoVEJSSSRpws2RefdrWbrFullV1Plot(
    Pds3Plot, dataset="CO-V/E/J/S/SS-RPWS-2-REFDR-WBRFULL-V1.0"
):
    """Class for the Cassini/RPWS Level 2 WBR-Full PDS3 dataset.

    PDS3 DATASET-ID: `CO-V/E/J/S/SS-RPWS-2-REFDR-WBRFULL-V1.0`."""

    pass


class CoVEJSSSRpws3RdrLrFullV1Plot(
    Pds3Plot, dataset="CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0"
):
    """Class for the Cassini/RPWS Level 3 LR-Full PDS3 dataset.

    PDS3 DATASET-ID: `CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0`."""

    pass
